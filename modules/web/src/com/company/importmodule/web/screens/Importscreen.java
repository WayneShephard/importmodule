package com.company.importmodule.web.screens;

import com.company.importmodule.service.DBExportService;
import com.company.importmodule.service.DeleteService;
import com.company.importmodule.service.ImportService;
import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.chile.core.model.MetaModel;
import com.haulmont.chile.core.model.MetaProperty;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.*;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import java.io.*;
import java.util.*;

import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.MetadataTools;

public class Importscreen extends AbstractWindow {

    String selected;
    ArrayList<MetaClass> metaClasses;

    @Inject ImportService importService;

    @Inject DeleteService deleteService;

    @Inject DBExportService dbExportService;

    @Inject com.haulmont.cuba.core.global.Messages messages;

    @Inject TextArea errorArea;
    @Inject TextArea infoArea;
    @Inject TextField pathField;
    @Inject LookupField rootPackageLookup;
    @Inject TwinColumn twinColumn;
    @Inject OptionsGroup actionOptionsGroup;
    @Inject OptionsGroup findTypeGroup;
    @Inject OptionsGroup analysOptionList;
    @Inject CheckBox overwriteCheck;
    //@Inject CheckBox byIdCheck;
    @Inject CheckBox alwaysCreateCheck;

    @Inject Metadata metadata;
    @Inject MetadataTools metadatatools;

    @Override
    public void init(Map<String, Object> params) {
        initComponents();
        twinColumn.addValueChangeListener(e -> {
            Collection collection = (Collection) e.getValue();
            String tempStr = "";
            for (Object item : collection) {
                if (!tempStr.contentEquals("")) {
                    tempStr += ",";
                }
                tempStr += ((MetaClass) item).getName();
            }
            selected = tempStr;
        });

        if(rootPackageLookup==null)
        {
            showNotification("Ошибка! Обратитесь к администратору.",NotificationType.ERROR);
            return;
        }
        Map mapRoot=new HashMap<String,Object>();
        ArrayList<MetaModel> packagesList=dbExportService.getRootPackages();
        for (MetaModel item: packagesList) {
            mapRoot.put(item.getName(),item);
        }
        rootPackageLookup.setOptionsMap(mapRoot);
        rootPackageLookup.addValueChangeListener(value->{
            //заполнение HashMap сущностями
            //Map map=new HashMap<String,Object>();
            MetaModel metaModel=rootPackageLookup.getValue();
            Map map = new HashMap();
            if(metaModel!=null) {
                //Object[] mcl = dbExportService.getEntities();
                Object[] mcl = dbExportService.getEntities(metaModel.getName());
                metaClasses = new ArrayList<>();

                for (Object obj : mcl) {
                    String regex = "\\$";
                    String temp[] = ((MetaClass) obj).getName().split(regex);
                    metaClasses.add((MetaClass) obj);
                    String metaClassName = messages.getMessage(((MetaClass) obj).getJavaClass(), temp[temp.length - 1]);
                    //map.put(temp[temp.length-1],(MetaClass)obj);
                    map.put(metaClassName, (MetaClass) obj);
                }
            }
            twinColumn.setOptionsMap(map);
        });

        selected = "";
    }

    public void onImportBtnClick() {
        String path = pathField.getRawValue();
        if (path.length() == 0) {
            showNotification("Введите значение пути!", NotificationType.WARNING);
        } else {

            boolean alwaysCreate=false;
            int findById;
            int merge=0;
            findById=getFindType();

            if(overwriteCheck.getValue()){
                merge=1;
            }
            if(alwaysCreateCheck.getValue()){
                alwaysCreate=true;
            }

            int assocActionType;
            assocActionType=getActionType();

            //вся логика импорта выполняется здесь
            ArrayList<String> errorList = importService.ImportData(path, selected, metaClasses,findById,merge,assocActionType,alwaysCreate);
            String temp = "";
            String lineSeparator = System.getProperty("line.separator");
            for (String error : errorList) {
                temp += error;
                temp += lineSeparator;
            }
            if(temp.contentEquals("")){
                temp+="Выполнено без ошибок";
            }
            errorArea.setValue(temp);

            String fileWriterError="Записано в " + path + "\\log.txt";
            if (!temp.contentEquals("")) {
                try {
                    FileWriter fileWriter = new FileWriter(path + "\\log.txt", false);
                    fileWriter.write(temp);
                    fileWriter.close();
                } catch (IOException e) {
                    fileWriterError="Ошибка записи лога "+e.getMessage();
                }
            }

            showNotification("Импорт", "Выполнено. Подробная информация доступна в логе", NotificationType.HUMANIZED);
        }
    }

    public void onDeleteBtnClick() {
        errorArea.setValue("");
        //ArrayList<String> errorList=null;
        Action act_0 = new DialogAction(DialogAction.Type.YES) {
            @Override
            public void actionPerform(Component component) {
                ArrayList<String> errorList = deleteService.deleteAll(metaClasses);
                String temp = "";
                String lineSeparator = System.getProperty("line.separator");
                for (String error : errorList) {
                    temp += error;
                    temp += lineSeparator;

                }
                errorArea.setValue(temp);

                if (errorList.size() != 0) {
                    showNotification("Удаление", "Ошибка удаления!", NotificationType.ERROR);
                } else {
                    showNotification("Удаление", "Удаление выполнено успешно", NotificationType.HUMANIZED);
                }
            }
        };
        Action act_1 = new DialogAction(DialogAction.Type.NO);
        Action[] act_array = new Action[2];
        act_array[0] = act_0;
        act_array[1] = act_1;
        showOptionDialog("Внимание!", "Вы уверены, что хотите удалить все данные?", MessageType.WARNING, act_array);
    }

    public void onDeleteEntityBtnClick() {
        ArrayList<String> errorList = deleteService.delete(selected, metaClasses);
        //TextArea textArea = (TextArea) getComponent("errorArea");
        String temp = "";
        for (String error : errorList) {
            temp += error;
            temp += "\n";

        }
        errorArea.setValue(temp);
        if (errorList.size() == 0) {
            showNotification("Удаление", "Выполнено без ошибок", NotificationType.HUMANIZED);
        } else {
            showNotification("Удаление", "Выполнено с ошибками", NotificationType.WARNING);
        }
    }

    public void onCheckBtnClick() {
        String path = pathField.getRawValue();
        if (path.length() == 0) {
            showNotification("Введите значение пути!", NotificationType.WARNING);
        } else {
            if(selected.contentEquals("")){
                showNotification("Выберите сущности для проверки",NotificationType.WARNING);
            }
            else {
                int findById=0;
                findById=getFindType();

                HashMap<String, ArrayList<String>> messageMap = importService.checkExsistence(path, selected, metaClasses,findById);
                String temp = "";
                String lineSeparator = System.getProperty("line.separator");
                String analysType = analysOptionList.getValue();
                ArrayList<String> messageList = null;
                switch (analysType) {
                    case "Дубликаты": {
                        messageList = messageMap.get("exist");
                        break;
                    }
                    case "Записи без конфликтов": {
                        messageList = messageMap.get("notExist");
                        break;
                    }
                }
                messageList.addAll(messageMap.get("error"));

                for (String message : messageList) {
                    temp += message;
                    temp += lineSeparator;

                }
                if (messageList.size() == 0) {
                    infoArea.setValue("Записи не найдены");
                } else {
                    infoArea.setValue(temp);
                }

                String fileWriterError = "Записано в " + path + "\\log.txt";
                if (!temp.contentEquals("")) {
                    try {
                        FileWriter fileWriter = new FileWriter(path + "\\log.txt", false);
                        fileWriter.write(temp);
                        fileWriter.close();
                    } catch (IOException e) {
                        fileWriterError = "Ошибка записи лога " + e.getMessage();
                    }
                }
            }
        }
    }

    private void initComponents()
    {
        actionOptionsGroup.setOrientation(OptionsGroup.Orientation.VERTICAL);
        ArrayList<String> optionsList=new ArrayList<>();
        //TODO ошибка при создании новой сущности по ключу, разрешить созданиесущности только по InstanceName
        optionsList.add("Уведомить");
        optionsList.add("Создать новую сущность");
        optionsList.add("Всегда создавать новую сущность");
        actionOptionsGroup.setOptionsList(optionsList);
        actionOptionsGroup.setValue("Уведомить");

        ArrayList<String> findTypeList=new ArrayList<>();
        findTypeList.add("Искать по наименованию");
        findTypeList.add("Искать по всем id");
        findTypeList.add("Искать по id основной сущности");
        findTypeGroup.setOptionsList(findTypeList);
        findTypeGroup.setValue("Искать по наименованию");

        ArrayList<String> analysOptionsList=new ArrayList<>();
        analysOptionsList.add("Дубликаты");
        analysOptionsList.add("Записи без конфликтов");
        analysOptionList.setOptionsList(analysOptionsList);
        analysOptionList.setValue("Дубликаты");
    }

    private boolean nullCheck(Component component)
    {
        if(component==null)
        {
            showNotification("Ошибка! Обратитесь к администратору.",NotificationType.ERROR);
            return false;
        }
        else{
            return true;
        }
    }

    public void onInfoFileBtnClick() {
        ArrayList<String> errorList=new ArrayList<>();
        if(selected.contentEquals("")){
            showNotification("Выберите сущность",NotificationType.WARNING);
            return;
        }
        String[] entities=selected.split(",");

        for(MetaClass metaClass:metaClasses) {
            for(String entityName:entities) {
                if (messages.getMessage(metaClass.getJavaClass(),metaClass.getName()).contentEquals(entityName)) {
                    try {
                        String path=importService.getInfoFile(metaClass,null,pathField.getRawValue());
                        showNotification("Шаблон создан и доступен по пути "+pathField.getRawValue()+"\\"+path,NotificationType.HUMANIZED);
                    }
                    catch (IOException e){
                        //TODO логгирование
                        showNotification("Ошибка создания шаблона",NotificationType.WARNING);
                    }
                }
            }
        }
    }

    public void onCustTemplClick() {
        openWindow("templateScreen", WindowManager.OpenType.DIALOG);
    }
    

    public void onSmartImportClick() {
        openWindow("smartImportScreen",WindowManager.OpenType.DIALOG);
    }

    private int getFindType(){
        String findType=findTypeGroup.getValue();
        if(findType==null){
            return 0;
        }
        else {
            switch (findType) {
                case "Искать по наименованию": {
                    return 0;
                }
                case "Искать по всем id": {
                    return 1;
                }
                case "Искать по id основной сущности": {
                    return 2;
                }
                default:{
                    return 0;
                }
            }
        }
    }

    private int getActionType(){
        String optionValue=actionOptionsGroup.getValue();
        if(optionValue==null){
            return 0;
        }
        else {
            switch (optionValue) {
                case "Уведомить": {
                    return 0;
                }
                case "Создать новую сущность": {
                    return 1;
                }
                case "Всегда создавать новую сущность": {
                    return 2;
                }
                default:{
                    return 0;
                }
            }
        }
    }


    public void onMetaModelBtnClick() {

        if (pathField.getRawValue().length() == 0) {
            showNotification("Введите значение пути!", NotificationType.WARNING);
            return;
        }

        if(rootPackageLookup.getValue()==null)
        {
            showNotification("Выберите Корневой проект!");
            return;
        }


        try {
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet1 = wb.createSheet("Entities");
            HSSFSheet sheet2 = wb.createSheet("Enumerations");
            String name = "MetaModel";
            FileOutputStream out = new FileOutputStream(new File(pathField.getRawValue() + "\\" + name + ".xls"));
            String path = name + ".xls";

            Row row;

            row = sheet1.createRow(0);
            row.createCell(0).setCellValue("Entity");
            row.createCell(1).setCellValue("Entity ru");
            row.createCell(2).setCellValue("Property");
            row.createCell(3).setCellValue("Property ru");
            row.createCell(4).setCellValue("Type");
            row.createCell(5).setCellValue("Cardinality");
            row.createCell(6).setCellValue("Mandatory");
            row.createCell(7).setCellValue("Readonly");
            row.createCell(8).setCellValue("Annotations");

            row = sheet2.createRow(0);
            row.createCell(0).setCellValue("Enumeration");
            row.createCell(1).setCellValue("Value");
            row.createCell(2).setCellValue("Value ru");


            MetaModel metaModel=rootPackageLookup.getValue();

            // Entities
            Object[] mcl = dbExportService.getEntities(metaModel.getName());
            int j=1;
            for (Object obj : mcl) {

                MetaClass metaClass = (MetaClass) obj;

                for (MetaProperty mpr : metaClass.getProperties()) {
                    row = sheet1.createRow(j);

                    // Entity
                    row.createCell(0).setCellValue(metaClass.getName());

                    // Entity ru
                    String temp[] = metaClass.getName().split("\\$");
                    String metaClassName = messages.getMessage(metaClass.getJavaClass(), temp[temp.length - 1]);
                    row.createCell(1).setCellValue(metaClassName);

                    // Property
                    row.createCell(2).setCellValue(mpr.getName());

                    // Property ru
                    String tempName = mpr.getName();
                    String[] tempName2 = (metaClass.getName() + "." + tempName).split("\\$");
                    String localName = messages.getMessage(metaClass.getJavaClass(), tempName2[tempName2.length - 1]);
                    row.createCell(3).setCellValue(localName);

                    // Type
                    row.createCell(4).setCellValue(mpr.getJavaType().getName());

                    // Cardinality
                    row.createCell(5).setCellValue(mpr.getRange().getCardinality().toString());

                    // Mandatory
                    row.createCell(6).setCellValue(mpr.isMandatory());

                    // Readonly
                    row.createCell(7).setCellValue(mpr.isReadOnly());

                    // Annotations
                    row.createCell(8).setCellValue(mpr.getAnnotations().toString());

                    j++;
                }
            }


            // Enumerations
            MetadataTools tools = metadata.getTools();
            j=1;
            for (Class enumClass:tools.getAllEnums()) {
                for (Object con : enumClass.getEnumConstants()) {

                    row = sheet2.createRow(j);

                    // Enumeration
                    row.createCell(0).setCellValue(enumClass.getName());

                    // Value
                    row.createCell(1).setCellValue(((Enum)con).name());

                    // Value ru
                    String[] enumName = enumClass.getName().split("\\.");
                    String enumLocalName = messages.getMessage(((Enum)con).getClass(), enumName[enumName.length - 1] + "." + ((Enum)con).name());
                    row.createCell(2).setCellValue(enumLocalName);

                    j++;
                }

            }


            wb.write(out);
            out.close();

            showNotification("Файл создан и доступен по пути " + pathField.getRawValue() + "\\" + path, NotificationType.HUMANIZED);

        }
        catch (IOException e){
            //TODO логгирование
            showNotification("Ошибка создания шаблона",NotificationType.WARNING);
        }

    }
    
}