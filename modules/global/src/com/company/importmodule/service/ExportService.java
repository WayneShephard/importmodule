package com.company.importmodule.service;


import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.chile.core.model.MetaProperty;

import java.io.IOException;
import java.util.ArrayList;

public interface ExportService {
    String NAME = "importmodule_ExportService";

    String getExportFile(MetaClass metaClass, ArrayList<MetaProperty> selectedProperties, int findType, boolean keyAndName, boolean withDict, String path) throws IOException;
    String getExportFileWithParam(MetaClass metaClass, ArrayList<MetaProperty> selectedProperties, int findType,
                                         boolean keyAndName, boolean withDict, String path, String parameter) throws IOException;
}