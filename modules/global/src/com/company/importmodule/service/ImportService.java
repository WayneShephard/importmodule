package com.company.importmodule.service;

import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.chile.core.model.MetaProperty;
import com.haulmont.cuba.core.entity.Entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface ImportService {
    String NAME = "importmodule_ImportService";

    ArrayList<String> ImportData(String path, String selected, ArrayList<MetaClass> metaClasses,int find,int merge, int assocActionType,boolean alwaysCreate);

    ArrayList<String> ImportDataWithParam(String path, String selected, ArrayList<MetaClass> metaClasses, int find, int merge,
                                          int assocActionType, boolean alwaysCreate,
                                          HashMap<String,String> param, HashMap<String,String> allowedValue,String parameter);

    HashMap<String,ArrayList<String>> checkExsistence(String path, String selected, ArrayList<MetaClass> metaClasses,int findType);

    String getInfoFile(MetaClass metaClass,ArrayList<MetaProperty> selectedProperties,String exportPath) throws IOException;

    String getImportTable(String path,MetaClass metaClass, ArrayList<MetaProperty> selectedProperties,int findType, String exportPath) throws IOException;

    boolean persistEntity(Entity entity);
}