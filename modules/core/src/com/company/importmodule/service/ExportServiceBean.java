package com.company.importmodule.service;

import com.company.importmodule.core.Import_work;
import com.haulmont.chile.core.datatypes.Enumeration;
import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.chile.core.model.MetaProperty;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.View;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

@Service(ExportService.NAME)
public class ExportServiceBean implements ExportService {

    @Inject
    Import_work import_work;

    @Inject
    Messages messages;

    @Inject
    DBExportService dbExportService;

    @Override
    public String getExportFile(MetaClass metaClass, ArrayList<MetaProperty> selectedProperties, int findType,
                                boolean keyAndName, boolean withDict, String path) throws IOException {
        String[] temp=metaClass.getName().split("\\$");
        String name=temp[temp.length-1];
        HashMap<String,Integer> propertyMap=new HashMap<>();
        HSSFWorkbook wb= import_work.getTemplateWorkbook(metaClass,selectedProperties,propertyMap);
        FileOutputStream out=new FileOutputStream(new File(path+"\\"+name + ".xls"));
        if(!withDict) {
            wb = addDataToWorkbook(wb, metaClass, selectedProperties, propertyMap, findType, keyAndName);
        }
        else{
            wb=addDataToWorkbookWithDictionaries(wb,metaClass,selectedProperties,propertyMap);
        }
        wb.write(out);
        out.close();
        return name+".xls";
    }

    @Override
    public String getExportFileWithParam(MetaClass metaClass, ArrayList<MetaProperty> selectedProperties, int findType,
                                boolean keyAndName, boolean withDict, String path, String parameter) throws IOException {
        String[] temp=metaClass.getName().split("\\$");
        String name=temp[temp.length-1];
        HashMap<String,Integer> propertyMap=new HashMap<>();
        HSSFWorkbook wb= import_work.getTemplateWorkbook(metaClass,selectedProperties,propertyMap);
        FileOutputStream out=new FileOutputStream(new File(path+"\\"+name + ".xls"));
        if(!withDict) {
            wb = addDataToWorkbook(wb, metaClass, selectedProperties, propertyMap, findType, keyAndName,parameter);
        }
        else{
            wb=addDataToWorkbookWithDictionaries(wb,metaClass,selectedProperties,propertyMap);
        }
        wb.write(out);
        out.close();
        return name+".xls";
    }



    private HSSFWorkbook addDataToWorkbook(HSSFWorkbook wb,MetaClass metaClass,
                                           ArrayList<MetaProperty> selectedProperties,HashMap<String,Integer> propertyMap, int findType,boolean keyAndName){
        ArrayList<View> views=new ArrayList<>();
        views.add(import_work.getFullView(metaClass));
        String table=metaClass.getName();
        Collection entityCollection=import_work.getAllEntitiesFromBD(metaClass,table,views);
        HSSFSheet sheet=wb.getSheetAt(0);
        Iterator<Row> iterator = sheet.iterator();
        int rowNum=3;
        Row row=sheet.createRow(rowNum);
        for(Object obj:entityCollection){
            for(MetaProperty metaProperty:selectedProperties){
                Object o=((Entity)obj).getValue(metaProperty.getName());
                String propertyValue="";
                if(metaProperty.getType()== MetaProperty.Type.ENUM){
                    if(o!=null) {
                        String temp = ((Enum) o).getClass().getName();
                        String[] enumName = ((Enum) o).getClass().getName().split("\\.");
                        String enumLocalName = messages.getMessage(((Enum) o).getClass(), enumName[enumName.length - 1] + "." + ((Enum) o).name());
                        propertyValue = enumLocalName;
                    }
                }
                else {
                    try {
                        Entity propEntity = (Entity) o;
                        if (keyAndName) {
                            propertyValue = propEntity.getId().toString() + " " + propEntity.getInstanceName();
                        } else {
                            if (findType == 1) {
                                propertyValue = propEntity.getId().toString();
                            } else {
                                propertyValue = propEntity.getInstanceName();
                            }
                        }
                    } catch (Exception e) {
                        propertyValue = String.valueOf(o);
                    }
                }
                String[] key = (metaClass.getName() + "." + metaProperty.getName()).split("\\$");
                String localName=messages.getMessage(metaClass.getJavaClass(),key[key.length - 1]);
                int cellNum=propertyMap.get(localName);
                //row.getCell(cellNum).setCellValue(propertyValue);
                row.createCell(cellNum).setCellValue(propertyValue);
            }
            ++rowNum;
            row=sheet.createRow(rowNum);
        }
        return wb;
    }

    private HSSFWorkbook addDataToWorkbook(HSSFWorkbook wb,MetaClass metaClass,
                                           ArrayList<MetaProperty> selectedProperties,HashMap<String,Integer> propertyMap, int findType,boolean keyAndName, String parameter){
        ArrayList<View> views=new ArrayList<>();
        views.add(import_work.getFullView(metaClass));
        String table=metaClass.getName();
        Collection entityCollection=import_work.getAllEntitiesFromBD(metaClass,table,views,parameter);
        HSSFSheet sheet=wb.getSheetAt(0);
        Iterator<Row> iterator = sheet.iterator();
        int rowNum=3;
        Row row=sheet.createRow(rowNum);
        for(Object obj:entityCollection){
            for(MetaProperty metaProperty:selectedProperties){
                Object o=((Entity)obj).getValue(metaProperty.getName());
                String propertyValue="";
                if(metaProperty.getType()== MetaProperty.Type.ENUM){
                    if(o!=null) {
                        String temp = ((Enum) o).getClass().getName();
                        String[] enumName = ((Enum) o).getClass().getName().split("\\.");
                        String enumLocalName = messages.getMessage(((Enum) o).getClass(), enumName[enumName.length - 1] + "." + ((Enum) o).name());
                        propertyValue = enumLocalName;
                    }
                }
                else {
                    try {
                        Entity propEntity = (Entity) o;
                        if (keyAndName) {
                            propertyValue = propEntity.getId().toString() + " " + propEntity.getInstanceName();
                        } else {
                            if (findType == 1) {
                                propertyValue = propEntity.getId().toString();
                            } else {
                                propertyValue = propEntity.getInstanceName();
                            }
                        }
                    } catch (Exception e) {
                        propertyValue = String.valueOf(o);
                    }
                }
                String[] key = (metaClass.getName() + "." + metaProperty.getName()).split("\\$");
                String localName=messages.getMessage(metaClass.getJavaClass(),key[key.length - 1]);
                int cellNum=propertyMap.get(localName);
                //row.getCell(cellNum).setCellValue(propertyValue);
                row.createCell(cellNum).setCellValue(propertyValue);
            }
            ++rowNum;
            row=sheet.createRow(rowNum);
        }
        return wb;
    }

    private HSSFWorkbook addDataToWorkbookWithDictionaries(HSSFWorkbook wb,MetaClass metaClass,
                                                           ArrayList<MetaProperty> selectedProperties,HashMap<String,Integer> propertyMap) {
        ArrayList<View> views = new ArrayList<>();
        views.add(import_work.getFullView(metaClass));
        String table = metaClass.getName();
        Collection entityCollection = import_work.getAllEntitiesFromBD(metaClass, table, views);
        HSSFSheet sheet = wb.getSheetAt(0);
        Iterator<Row> iterator = sheet.iterator();
        int rowNum = 3;
        Row row = sheet.createRow(rowNum);
        for (Object obj : entityCollection) {
            for (MetaProperty metaProperty : selectedProperties) {
                Object o = ((Entity) obj).getValue(metaProperty.getName());
                String propertyValue = "";
                if (metaProperty.getType() == MetaProperty.Type.ENUM) {
                    if (o != null) {
                        String temp = ((Enum) o).getClass().getName();
                        String[] enumName = ((Enum) o).getClass().getName().split("\\.");
                        String enumLocalName = messages.getMessage(((Enum) o).getClass(), enumName[enumName.length - 1] + "." + ((Enum) o).name());
                        propertyValue = enumLocalName;
                    }
                } else {
                    try {
                        Entity propEntity = (Entity) o;
                        propertyValue = propEntity.getId().toString();

                        /*Sheet tempSheet=wb.getSheet(propEntity.getMetaClass().getName());
                        if(tempSheet==null) {
                            wb.createSheet(propEntity.getMetaClass().getName());
                        }*/
                        ArrayList<MetaProperty> metaPropertyList=new ArrayList<>();
                        metaPropertyList.addAll(propEntity.getMetaClass().getOwnProperties());
                        HashMap<String,Integer> tempMap=new HashMap<>();
                        import_work.addTemplateToWorkbook(wb,propEntity.getMetaClass(),metaPropertyList,tempMap);
                        addOneEntityToWorkbook(wb,propEntity.getMetaClass(),propEntity,rowNum);
                    } catch (Exception e) {
                        propertyValue = String.valueOf(o);
                    }
                }
                String[] key = (metaClass.getName() + "." + metaProperty.getName()).split("\\$");
                String localName = messages.getMessage(metaClass.getJavaClass(), key[key.length - 1]);
                int cellNum = propertyMap.get(localName);
                //row.getCell(cellNum).setCellValue(propertyValue);
                row.createCell(cellNum).setCellValue(propertyValue);
            }
            ++rowNum;
            row = sheet.createRow(rowNum);
        }
        return wb;
    }

    private HSSFWorkbook addOneEntityToWorkbook(HSSFWorkbook wb, MetaClass metaClass, Entity entity, int rowNum){
        ArrayList<MetaProperty> metaPropertyList=new ArrayList<>();
        metaPropertyList.addAll(metaClass.getOwnProperties());
        Sheet sheet=wb.getSheet(metaClass.getName());
        Row row=sheet.createRow(rowNum);
        for(int i=0;i<metaPropertyList.size();++i){
            String propertyValue="";
            Cell cell=row.createCell(i);
            Object o=entity.getValue(metaPropertyList.get(i).getName());
            try {
                Entity propEntity = (Entity) o;
                propertyValue = propEntity.getInstanceName();
            } catch (Exception e) {
                propertyValue = String.valueOf(o);
            }
            cell.setCellValue(propertyValue);
        }
        return wb;
    }



}

