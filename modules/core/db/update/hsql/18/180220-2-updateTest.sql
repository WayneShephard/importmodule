alter table IMPORTMODULE_TEST add column MANDATORY varchar(255) ^
update IMPORTMODULE_TEST set MANDATORY = '' where MANDATORY is null ;
alter table IMPORTMODULE_TEST alter column MANDATORY set not null ;
